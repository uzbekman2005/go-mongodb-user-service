package db

import (
	"context"

	"gitlab.com/uzbekman2005/go-mongodb-user-service/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func ConnectToDb(cfg config.Config) (*mongo.Client, error) {
	return mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://localhost:27017"))
}
