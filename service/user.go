package service

import (
	"context"

	"gitlab.com/uzbekman2005/go-mongodb-user-service/pkg/logger"
	"gitlab.com/uzbekman2005/go-mongodb-user-service/storage"
	"go.mongodb.org/mongo-driver/mongo"

	pb "gitlab.com/uzbekman2005/go-mongodb-user-service/genproto/user"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	Storage storage.IStorage
	Logger  logger.Logger
}

func NewUserService(db *mongo.Client, l logger.Logger) *UserService {
	return &UserService{
		Storage: storage.NewStoragePg(db),
		Logger:  l,
	}
}

func (s *UserService) CreateUser(ctx context.Context, req *pb.User) (*pb.User, error) {
	res, err := s.Storage.User().Create(req)
	if err != nil {
		s.Logger.Error("Error while inserting into users", logger.Any("Insert", err))
		return &pb.User{}, status.Error(codes.Internal, "Something went wrong please check user info")
	}
	return res, nil
}

func (s *UserService) GetUsersById(ctx context.Context, in *pb.Ids) (*pb.GetUsers, error) {
	res, err := s.Storage.User().GetUsersById(in)
	if err != nil {
		s.Logger.Error("ERROR WHILE GETTING USERS", logger.Any("get", err))
		return &pb.GetUsers{}, status.Error(codes.NotFound, "Some of the users you ask are not exist")
	}
	return res, nil
}

func (s *UserService) UpdateUser(ctx context.Context, in *pb.User) (*pb.User, error) {
	res, err := s.Storage.User().UpdateUser(in)
	if err != nil {
		s.Logger.Error("Error while updating", logger.Any("Update", err))
		return &pb.User{}, status.Error(codes.InvalidArgument, "Please recheck user info")
	}
	return res, nil
}

func (s *UserService) DeleteUser(ctx context.Context, in *pb.Id) (*pb.Empty, error) {
	err := s.Storage.User().DeletUser(in)
	if err != nil {
		s.Logger.Error("Error while deleting", logger.Any("Delete", err))
		return &pb.Empty{}, status.Error(codes.InvalidArgument, "Some of the users that you are going to delete are not exist")
	}
	return &pb.Empty{}, nil
}
