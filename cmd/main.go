package main

import (
	"context"
	"net"

	"gitlab.com/uzbekman2005/go-mongodb-user-service/config"
	pb "gitlab.com/uzbekman2005/go-mongodb-user-service/genproto/user"
	"gitlab.com/uzbekman2005/go-mongodb-user-service/pkg/db"
	"gitlab.com/uzbekman2005/go-mongodb-user-service/pkg/logger"
	"gitlab.com/uzbekman2005/go-mongodb-user-service/service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "user-service")
	defer logger.CleanUp(log)

	connDB, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("Error while connecting to database", logger.Error(err))
	}
	if err != nil {
		log.Fatal("Error while connecting to mongodb", logger.Error(err))
	}
	defer connDB.Disconnect(context.Background())

	userService := service.NewUserService(connDB, log)

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listening 1: %v", logger.Error(err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	pb.RegisterUserServiceServer(c, userService)
	log.Info("Server is running",
		logger.String("port", cfg.RPCPort))

	if err := c.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
