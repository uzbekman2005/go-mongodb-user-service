package postgres

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	pb "gitlab.com/uzbekman2005/go-mongodb-user-service/genproto/user"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type UserRepo struct {
	Db *mongo.Client
}

func NewUserRepo(db *mongo.Client) *UserRepo {
	return &UserRepo{Db: db}
}

func (r *UserRepo) Create(user *pb.User) (*pb.User, error) {
	collection := r.Db.Database("user_service").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	user.Id = uuid.New().String()
	_, err := collection.InsertOne(ctx, bson.D{
		{Key: "id", Value: user.Id},
		{Key: "firstname", Value: user.FirstName},
		{Key: "lastname", Value: user.LastName},
		{Key: "email", Value: user.Email},
		{Key: "phones", Value: user.Phones},
	})
	if err != nil {
		return &pb.User{}, err
	}
	return user, err
}

func (r *UserRepo) GetUsersById(ids *pb.Ids) (*pb.GetUsers, error) {
	collection := r.Db.Database("user_service").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	response := &pb.GetUsers{}
	for _, id := range ids.Ids {
		res := &pb.User{}
		filter := bson.D{{Key: "id", Value: id}}
		err := collection.FindOne(ctx, filter).Decode(&res)
		if err != nil {
			return &pb.GetUsers{}, err
		}
		response.Users = append(response.Users, res)
	}
	return response, nil
}

func (r *UserRepo) UpdateUser(usr *pb.User) (*pb.User, error) {
	collection := r.Db.Database("user_service").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// res := &pb.User{}
	result, err := collection.UpdateOne(ctx, bson.M{"id": usr.Id}, bson.D{
		{Key: "$set", Value: bson.D{{Key: "firstname", Value: usr.FirstName}}},
	})

	if err != nil {
		return &pb.User{}, err
	}
	fmt.Println(result.ModifiedCount)

	result, err = collection.ReplaceOne(ctx, bson.M{"id": usr.Id},
		bson.M{
			"id":        usr.Id,
			"firstname": usr.FirstName,
			"lastname":  usr.LastName,
			"email":     usr.Email,
			"phones":    usr.Phones,
		},
	)
	if err != nil {
		return &pb.User{}, err
	}
	fmt.Println(result.ModifiedCount)
	return usr, nil
}

func (r *UserRepo) DeletUser(id *pb.Id) error {
	collection := r.Db.Database("user_service").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	res, err := collection.DeleteOne(ctx, bson.M{"id": id.Id})
	if err != nil {
		return err
	}
	fmt.Println(id.Id)
	fmt.Println("Deleted count := ", res.DeletedCount)
	return nil
}
