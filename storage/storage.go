package storage

import (
	"gitlab.com/uzbekman2005/go-mongodb-user-service/storage/postgres"
	"gitlab.com/uzbekman2005/go-mongodb-user-service/storage/repo"
	"go.mongodb.org/mongo-driver/mongo"
)

type IStorage interface {
	User() repo.UserStorageI
}

type StoragePg struct {
	Db       *mongo.Client
	userRepo repo.UserStorageI
}

// NewStoragePg
func NewStoragePg(db *mongo.Client) *StoragePg {
	return &StoragePg{
		Db:       db,
		userRepo: postgres.NewUserRepo(db),
	}
}

func (s StoragePg) User() repo.UserStorageI {
	return s.userRepo
}
