package repo

import (
	pb "gitlab.com/uzbekman2005/go-mongodb-user-service/genproto/user"
)

type UserStorageI interface {
	Create(*pb.User) (*pb.User, error)
	GetUsersById(*pb.Ids) (*pb.GetUsers, error)
	UpdateUser(*pb.User) (*pb.User, error)
	DeletUser(*pb.Id) error
}
